@echo off
if "%VisualStudioVersion%"=="" goto Err_vcvars
echo VisualStudioVersion=%VisualStudioVersion%
set OUT_DIR=.\build
if not exist %OUT_DIR% mkdir %OUT_DIR%
cl /Ox /MD /LD /EHsc /DRF625_LIBRARY /Fe:%OUT_DIR%\rf625 src\rf625.cpp src\sockfun.cpp src\wshlp.cpp /link /DLL
cl /Ox /MD /EHsc /Isrc /Fe:%OUT_DIR%\udp_stream samples\udp_stream.cpp %OUT_DIR%\rf625.lib ws2_32.lib
cl /Ox /MD /EHsc /Isrc /Fe:%OUT_DIR%\udp_rate samples\udp_rate.cpp %OUT_DIR%\rf625.lib ws2_32.lib
del *.obj
goto Exit
:Err_vcvars
echo Please call vcvarsall.bat first
echo (e.g., `"C:\Program Files (x86)\Microsoft Visual Studio\2017\Community\VC\Auxiliary\Build\vcvarsall.bat" x64`)
:Exit
