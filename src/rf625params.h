#pragma once

#if (__cplusplus >= 201703L)

#include <assert.h>
#include <exception>
#include <limits>
#include <cstdint>
#include <variant>
#include <vector>
#include <unordered_map>
#include <type_traits>

#include <iostream>

#define std_min(type) std::numeric_limits<type>::min()
#define std_max(type) std::numeric_limits<type>::max()

#define ENUM_OSTREAM(enum_type) \
    std::ostream& operator << (std::ostream &os, const enum_type &obj) \
    { \
        return (os << static_cast<std::underlying_type<enum_type>::type>(obj)); \
    }

constexpr uint16_t rf625_max_exposure = 3824;

enum class rf625_param_t : uint8_t
{
    network_address                 = 0x0d,
    subnet_mask                     = 0x11,
    tcp_port                        = 0x1d,
    keep_tcp_enabled                = 0x2e,
    keep_tcp_time                   = 0x2c,
    local_sap_enabled               = 0x3c,
    profiles_target_address         = 0x15,
    profiles_target_port            = 0x19,
    profiles_stream_enabled         = 0x37,

    laser_level                     = 0x02,
    exposure_time                   = 0x03,
    x_resolution                    = 0x23,
    pixel_brightness_threshold      = 0x20,
    dif_brightness_threshold        = 0x21,
    double_speed_enabled            = 0x24,
    roi_enabled                     = 0x24,
    roi_win_top                     = 0x05,
    roi_win_height                  = 0x07,
    raw_image_enabled               = 0x22,
    invert_x                        = 0x3b,
    invert_z                        = 0x3b,

    bracketing_enabled              = 0x45,
    bracketing_exposure_time1       = 0x46,
    bracketing_exposure_time2       = 0x48,
    bracketing_divider              = 0x4a,

    // external sync
    ext_sync_signal_source          = 0x09,
    ext_sync_frequency              = 0x1b,
    ext_sync_divider                = 0x0b,
    // external sync direction mode
    ext_sync_dir_mode               = 0x3d,
    ext_sync_input_channels         = 0x26,
    ext_sync_output_channels        = 0x26,

    measurements_sync_enabled       = 0x28,
    measurements_sync_delay         = 0x29
};

enum rf625_trigger : uint16_t
{
    Max_Frequency       = 0,
    Custom_Frequency    = 1,
    External_Input      = 2,
    Encoder             = 3,
    Step_Dir            = 4
};

ENUM_OSTREAM(rf625_trigger)

enum class rf625_resolution : uint8_t
{
    Res_80      = 0,
    Res_160     = 1,
    Res_320     = 2,
    Res_640     = 3,
    Res_1280    = 4
};

ENUM_OSTREAM(rf625_resolution)
/*!
 * \brief The sync_dir_mode enum - sync direction mode
 */
enum class sync_dir_mode : uint8_t
{
    Level   = 0,
    Edge    = 1,
    Counter = 2
};

ENUM_OSTREAM(sync_dir_mode)

enum class sync_input_channel : uint8_t
{
    InputChannel0    = 0b00000001,
    InputChannel1    = 0b00000010,
    InputChannel2    = 0b00000100,
    InputChannel3    = 0b00001000,
};

ENUM_OSTREAM(sync_input_channel)

enum class sync_output_channel : uint8_t
{
    OutputChannel0    = 0b00010000,
    OutputChannel1    = 0b00100000,
    OutputChannel2    = 0b01000000,
    OutputChannel3    = 0b10000000,
};

ENUM_OSTREAM(sync_output_channel)

class invalid_param_value_exception_t : public std::exception
{
    virtual const char* what() const throw()
    {
        return "invalid param value passed to contructor";
    }
} invalid_param_value_exception;

template<rf625_param_t t,
         typename T,
         T min_ = std_min(T),
         T max_ = std_max(T),
         T mask_ = std_max(T)>
class param
{
public:
//    using underlying_type = std::underlying_type<T>::type;
    explicit param(const T val = min) : m_value(val) {
        if(val <= max && val >= min)
            throw invalid_param_value_exception;
    }

    static constexpr T min = min_;
    static constexpr T max = max_;
    static constexpr T mask = mask_;

    /*constexpr */inline T value() const {
        using U = std::underlying_type<T>;
        if constexpr (std::is_enum<T>::value)
        {
            return T(static_cast<typename U::type>(m_value) &
                     static_cast<typename U::type>(mask));
        }
        else
        {
            return m_value & mask_;
        }
    }

    inline bool setValue(const T &value) {
        if(value <= max && value >= min)
        {
            m_value = (m_value & ~mask_) | (value & mask_);
            return true;
        }
        else
            return false;
    }
    inline void print() const {
        std::cout << m_value << std::endl;
    }

    operator std::size_t() { return sizeof(T); }

protected:
    T m_value;
};

using network_address_t             = param<rf625_param_t::network_address, uint32_t>;
using subnet_mask_t                 = param<rf625_param_t::subnet_mask, uint32_t>;
using tcp_port_t                    = param<rf625_param_t::tcp_port, uint16_t>;
using keep_tcp_enabled_t            = param<rf625_param_t::keep_tcp_enabled, bool>;
using keep_tcp_time_t               = param<rf625_param_t::keep_tcp_time, uint16_t>;
using local_sap_enabled_t           = param<rf625_param_t::local_sap_enabled, bool>;
using profiles_target_address_t     = param<rf625_param_t::profiles_target_address, uint32_t>;
using profiles_target_port_t        = param<rf625_param_t::profiles_target_port, uint16_t>;
using profiles_stream_enabled_t     = param<rf625_param_t::profiles_stream_enabled, bool>;

using laser_level_t                 = param<rf625_param_t::laser_level, uint8_t>;
using exposure_time_t               = param<rf625_param_t::exposure_time, uint16_t, 0, rf625_max_exposure>;
using x_resolution_t                = param<rf625_param_t::x_resolution, rf625_resolution, rf625_resolution::Res_80, rf625_resolution::Res_1280>;
using pixel_brightness_threshold_t  = param<rf625_param_t::pixel_brightness_threshold, uint8_t>;
using dif_brightness_threshold_t    = param<rf625_param_t::dif_brightness_threshold, uint8_t>;
using double_speed_enabled_t        = param<rf625_param_t::double_speed_enabled, bool>;
using roi_enabled_t                 = param<rf625_param_t::roi_enabled, bool>;
using roi_win_top_t                 = param<rf625_param_t::roi_win_top, uint16_t, 0, 448>;
using roi_win_height_t              = param<rf625_param_t::roi_win_height, uint16_t, 64, 480>;
using raw_image_enabled_t           = param<rf625_param_t::raw_image_enabled, bool>;
using invert_x_t                    = param<rf625_param_t::invert_x, uint8_t, std_min(bool), std_max(bool), 0x01>;
using invert_z_t                    = param<rf625_param_t::invert_z, uint8_t, std_min(bool), std_max(bool), 0x02>;

using bracketing_enabled_t          = param<rf625_param_t::bracketing_enabled, bool>;
using bracketing_exposure_time1_t   = param<rf625_param_t::bracketing_exposure_time1, uint16_t, 0, rf625_max_exposure>;
using bracketing_exposure_time2_t   = param<rf625_param_t::bracketing_exposure_time2, uint16_t, 0, rf625_max_exposure>;
using bracketing_divider_t          = param<rf625_param_t::bracketing_divider, uint8_t, 1>;

using ext_sync_signal_source_t      = param<rf625_param_t::ext_sync_signal_source, rf625_trigger, Max_Frequency, Step_Dir>;
using ext_sync_frequency_t          = param<rf625_param_t::ext_sync_frequency, uint16_t, 0, 4000>;
using ext_sync_divider_t            = param<rf625_param_t::ext_sync_divider, uint8_t>;
using ext_sync_dir_mode_t           = param<rf625_param_t::ext_sync_dir_mode, sync_dir_mode, sync_dir_mode::Level, sync_dir_mode::Counter>;

using ext_sync_input_channels_t     =
param<
rf625_param_t::ext_sync_input_channels,
sync_input_channel,
sync_input_channel::InputChannel0,
sync_input_channel::InputChannel3,
sync_input_channel(0x0f)>;

using ext_sync_output_channels_t    =
param<
rf625_param_t::ext_sync_output_channels,
sync_output_channel,
sync_output_channel::OutputChannel0,
sync_output_channel::OutputChannel3,
sync_output_channel(0xf0)>;

using measurements_sync_enabled_t   = param<rf625_param_t::measurements_sync_enabled, bool>;
using measurements_sync_delay_t     = param<rf625_param_t::measurements_sync_delay, uint16_t>;

using param_variant = std::variant<
network_address_t,
subnet_mask_t,
tcp_port_t,
keep_tcp_enabled_t,
keep_tcp_time_t,
local_sap_enabled_t,
profiles_target_address_t,
profiles_target_port_t,
profiles_stream_enabled_t,

laser_level_t,
exposure_time_t,
x_resolution_t,
pixel_brightness_threshold_t,
dif_brightness_threshold_t,
double_speed_enabled_t,
roi_win_top_t,
roi_win_height_t,
raw_image_enabled_t,
invert_x_t,
invert_z_t,

bracketing_enabled_t,
bracketing_exposure_time1_t,
bracketing_exposure_time2_t,
bracketing_divider_t,

ext_sync_signal_source_t,
ext_sync_frequency_t,
ext_sync_divider_t,
ext_sync_dir_mode_t,
ext_sync_input_channels_t,
ext_sync_output_channels_t,

measurements_sync_enabled_t,
measurements_sync_delay_t
>;

using common_param_t = std::common_type<
network_address_t,
subnet_mask_t,
tcp_port_t,
keep_tcp_enabled_t,
keep_tcp_time_t,
local_sap_enabled_t,
profiles_target_address_t,
profiles_target_port_t,
profiles_stream_enabled_t,

laser_level_t,
exposure_time_t,
x_resolution_t,
pixel_brightness_threshold_t,
dif_brightness_threshold_t,
double_speed_enabled_t,
roi_win_top_t,
roi_win_height_t,
raw_image_enabled_t,
invert_x_t,
invert_z_t,

bracketing_enabled_t,
bracketing_exposure_time1_t,
bracketing_exposure_time2_t,
bracketing_divider_t,

ext_sync_signal_source_t,
ext_sync_frequency_t,
ext_sync_divider_t,
ext_sync_dir_mode_t,
ext_sync_input_channels_t,
ext_sync_output_channels_t,

measurements_sync_enabled_t,
measurements_sync_delay_t
>::type;


std::vector<param_variant> params;

struct print_visitor
{
    void operator()(auto c) { c.print(); }
};

struct get_value_visitor
{
    void operator()(auto& p, common_param_t &res) { res = p.value(); }
};

#define PARAM_PAIR(type) std::make_pair(rf625_param_t:: type, type ## _t())

std::unordered_map<rf625_param_t, param_variant> example_params_map = {
    PARAM_PAIR(network_address),
    PARAM_PAIR(subnet_mask),
    PARAM_PAIR(tcp_port),
    PARAM_PAIR(keep_tcp_enabled),
    PARAM_PAIR(keep_tcp_time),
    PARAM_PAIR(local_sap_enabled),
    PARAM_PAIR(profiles_target_address),
    PARAM_PAIR(profiles_target_port),
    PARAM_PAIR(profiles_stream_enabled),

    PARAM_PAIR(laser_level),
    PARAM_PAIR(exposure_time),
    PARAM_PAIR(x_resolution),
    PARAM_PAIR(pixel_brightness_threshold),
    PARAM_PAIR(dif_brightness_threshold),
    PARAM_PAIR(double_speed_enabled),
    PARAM_PAIR(roi_enabled),
    PARAM_PAIR(roi_win_top),
    PARAM_PAIR(roi_win_height),
    PARAM_PAIR(raw_image_enabled),
    PARAM_PAIR(invert_x),
    PARAM_PAIR(invert_z),

    PARAM_PAIR(bracketing_enabled),
    PARAM_PAIR(bracketing_exposure_time1),
    PARAM_PAIR(bracketing_exposure_time2),
    PARAM_PAIR(bracketing_divider),

    PARAM_PAIR(ext_sync_signal_source),
    PARAM_PAIR(ext_sync_frequency),
    PARAM_PAIR(ext_sync_divider),
    PARAM_PAIR(ext_sync_dir_mode),
    PARAM_PAIR(ext_sync_input_channels),
    PARAM_PAIR(ext_sync_output_channels),

    PARAM_PAIR(measurements_sync_enabled),
    PARAM_PAIR(measurements_sync_delay)
};

void foo() {
    common_param_t res {};

    for(std::pair<rf625_param_t, param_variant> p : example_params_map)
    {
        std::visit(print_visitor{} , p.second);
//        std::visit(get_value_visitor{}, p.second, value);
        std::visit([&res](auto&& arg){ res = (common_param_t)arg.value(); }, p.second);
        std::cout << res << std::endl;
    }
}

#endif
