/*
 *  File name:  librf625.h
 *
 *  Description:    Interface library for Riftek RF625 2D Laser Scanner
 *
 *  Updated:    2018/01/20
 *
 *  Change history:
 *
 *  2018/01/20
 *  Implement unconnected protocol API
 *  Change validate_params to assign invalid parameters bit mask to error_bits
 *  rf625::search now creates rf625 objects and returns it as list of shared pointers
 *  Add get_info() method
 *
 *  2018/01/19
 *  Add UDP callbacks
 *  Remove note regarding some profile attributes are only used for udp
 *  Implement validate_params
 *  Remove implementation and disable copy constructor, disable default constructor
 *
 */
#pragma once

#ifdef _WIN32
#include <winsock.h>
#else
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#define INVALID_SOCKET (-1)
typedef int SOCKET;
#endif
#include <cstdint>
#include <vector>
#include <thread>
#include <mutex>
#include <string.h>

#include "rf625params.h"

#define RF625_API_VERSION                   (0x00040000)

#if (defined _WIN32 && defined RF625_LIBRARY && !defined TEST)
#define RF625_API __declspec(dllexport)
#else
#define RF625_API
#endif

/*! Structure containing RF625 information
 */
enum rf625_laser_color : unsigned char
{
    Red         = 1,
    Blue        = 2,
    Green       = 3,
    Infrared    = 4
};

struct rf625_info
{
    uint8_t hardware_address[6];            //!< MAC Address
    uint32_t ip_address;                    //!< IP Address
    uint32_t subnet_mask;                   //!< Subnet mask
    u_short tcp_port;                       //!< TCP port number
    u_short udp_port;                       //!< UDP port number
    bool dhs_capable;                       //!< Is scanner DHS capable
    rf625_laser_color laser_color;          //!< Laser beam color
    uint32_t serial_number;                 //!< Serial number
    uint16_t z_begin;                       //!< Begin of Z range, mm
    uint16_t z_range;                       //!< Range of Z, mm
    uint16_t x_smr;                         //!< Range of X (SMR), mm
    uint16_t x_emr;                         //!< Range of X (EMR), mm
    uint32_t uimage_version;                //!< uImage version
    uint32_t core_a_version;                //!< Core A version
    uint32_t core_b_version;                //!< Core B version
    uint32_t fpga_version;                  //!< FPGA version
    uint32_t uptime;                        //!< System uptime, seconds
    bool tcp_connection_active;             //!< TCP connection is active?
};

struct rf625_parameters
{
    bool laser_enable;                      //!< Enable laser trigger
    uint8_t laser_level;                    //!< Laser level [0..255]
    uint16_t exposure_time;                 //!< Exposure time, usec [0..3824]
    uint16_t roi_top;                       //!< Top position of ROI window, px
    uint16_t roi_height;                    //!< Height of ROI window, px
    rf625_trigger trigger;                  //!< Trigger
    uint16_t ext_sync_divisor;              //!< External sync signal divisor (0 or 1 is off)
    uint32_t ip_address;                    //!< IP address
    uint32_t subnet_mask;                   //!< Subnet mask
    uint32_t udp_host_address;              //!< UDP host address
    u_short udp_port;                       //!< UDP port number
    uint16_t udp_frequency;                 //!< Frequency, Hz of UDP stream (when Trigger set to Custom freq.)
    u_short tcp_port;                       //!< TCP port number
    uint8_t	pixel_brightness_thres;         //!< Threshold level for pixel brightness filter
    uint8_t	diff_brightness_thres;          //!< Threshold level for differential brightness filter
    bool raw_image;                         //!< Raw (unprocessed) image mode
    rf625_resolution resolution;            //!< Measured profile resolution in points
    bool dhs_enable;                        //!< Enable DHS mode
    uint8_t sync_input_channel;             //!< Input channel for external input (0 is off or [1..4])
    bool measure_sync_enable;               //!< Enable measurement sync
    uint8_t measurement_sync_input_channel; //!< Input channel for external input for measurement sync (0 is off or [1..4])
    uint16_t measurement_sync_delay;        //!< Measurement sync delay, usec
    uint8_t output_channel;                 //!< Output channel (0 is off or [1..2])
    bool tcp_autodisconnect_enable;         //!< Enable autodisconnection of inactive TCP session
    uint16_t tcp_autodisconnect_timeout;    //!< Timeout to autodisconnet inactive TCP session, sec (0 is off)
    bool udp_stream_enable;                 //!< Enable UDP stream
    bool encoder_has_zero;                  //!< Encoder has zero?
    bool bracketing_enable;                 //!< Enable bracketing mode (experimental)
    uint16_t bracketing_exposure_0;         //!< Exposure values for even frames, usec
    uint16_t bracketing_exposure_1;         //!< Exposure values for odd frames, usec
    bool fixed_frame_enable;                //!< Enable fixed frame mode
};

enum rf625_parameter_bits: uint32_t
{
    Laser_Enable = 0x00000001,
    Laser_Level = 0x00000002,
    Exposure_Time = 0x00000004,
    ROI_Top = 0x00000008,
    ROI_Height = 0x00000010,
    Trigger = 0x00000020,
    Ext_Sync_Divisor = 0x00000040,
    IP_Address = 0x00000080,
    Subnet_Mask = 0x00000100,
    UDP_Host_Address = 0x00000200,
    UDP_Port = 0x00000400,
    UDP_Frequency = 0x00000800,
    TCP_Port = 0x00001000,
    Pixel_Brightness_Thres = 0x00002000,
    Diff_Brightness_Thres = 0x00004000,
    Raw_Image = 0x00008000,
    Resolution = 0x00010000,
    DHS_Enable = 0x00020000,
    Sync_Input_Channel = 0x00040000,
    Measure_Sync_Enable = 0x00080000,
    Measurement_Sync_Input_Channel = 0x00100000,
    Measurement_Sync_Delay = 0x00200000,
    Output_Channel = 0x00400000,
    Tcp_Autodisconnect_Enable = 0x00800000,
    Tcp_Autodisconnect_Timeout = 0x01000000,
    UDP_Stream_Enable = 0x02000000,
    Encoder_Has_Zero = 0x04000000,
    Bracketing_Enable = 0x08000000,
    Bracketing_Exposure_0 = 0x10000000,
    Bracketing_Exposure_1 = 0x20000000,
    Fixed_Frame_Enable = 0x40000000,
    Params_Bits_All = 0xFFFFFFFF
};

/*! Structure to store a point of profile
 */
struct rf625_point
{
    double x;
    double z;
};

/*! Structure to store a profile
 */
struct rf625_profile
{
    std::vector<rf625_point> points;        //!< Points of profile
    uint16_t measure_cnt;                   //!< Measurement counter (or Step value in StepDir trigger mode)
    uint16_t packet_cnt;                    //!< Packet counter
    uint32_t timestamp;                     //!< Timestamp set upon packet send
    uint16_t dir;                           //!< Dir value in StepDir trigger mode
    uint32_t serial_number;                 //!< Serial number of scanner
    uint32_t source_ip;                     //!< Source IP address
    uint16_t source_port;                   //!< Source port number
    uint16_t destination_port;              //!< Destination port number
    uint16_t x_emr;                         //!< Range of X (EMR), mm
    uint16_t z_range;                       //!< Range of Z, mm
};

class rf625;
using rf625_list = std::vector<std::shared_ptr<rf625>>;

typedef void (*rf625_udp_proc)(rf625_profile profile);

class RF625_API rf625
{
public:

    rf625& operator =(const rf625&) = delete;

    /*! TCP functions
     */

    /*! Must be called once before using of library
     * \return true on success
     */
    static bool init();
    /*! Should be called to free resources allocated by init()
     */
    static void cleanup();
    /*! Search for instances of RF625 over network
     * \param ntime     :   search time in milliseconds
     * \return rf625_list - list of rf625 objects for each scanner found
     */
	static rf625_list search(int time_ms);
    /*! rf625 constructor
     * \param info      :   rf625_info structure
     */
    explicit rf625(const rf625_info& info);
    ~rf625();
    /*! Returns const ref to rf625_info structure
     */
	const rf625_info& get_info() const;
    /*! Start TCP connection to scanner
     * \return true on success
     */
    bool connect();
    /*!
     * Close TCP connection to scanner
     * \return
     */
    bool disconnect();
    /*! Check connection state
     * \return true if connected
     */
    inline bool is_connected() const {return (m_sock != INVALID_SOCKET);}
    /*! Read user parameters from scanner's memory
     * \param params    :   a reference to rf625_params structure being filled
     * \return true on success
     */
    bool read_params(rf625_parameters& params);
    /*! Validate rf625_parameters structure
     * \param params    :   a reference to rf625_params structure
     * \param error_bits:   a pointer to bit mask of invalid parameters (optional)
     *                      for each invalid value its bit set to 1 according to rf625_parameter_bits
     * \return true if all values are valid
     */
    bool validate_params(const rf625_parameters& params, uint32_t* error_bits = nullptr);
    /*! Transfer user parameters to scanner's memory
     * \param params    :   a reference to rf625_params structure to transfer
     * \param error_bits:   pointer to uint32_t bit mask containing 1 for each error parameter
     *                      each bit represented with rf625_parameter_bits enum value
     * \return true on success
     */
    bool write_params(const rf625_parameters& params);
    /*! Write user parameters from scanner's memory to flash
     * \return true on success
     */
    bool flush_params();
    /*! Reset user parameters to factory
     * \return true on success
     */
    bool reset_params();
    /*! Get result profile by TCP request
     * \param profile   :   a reference to result rf625_profile structure
     * \return true on success
     */
    bool get_result(rf625_profile& profile);
    /*!
     * \param ppixmap   :   pointer to pixmap array (640 * 480 bytes, 8 bit per pixel)
     * \return true on success
     */
    bool get_image(uint8_t* ppixmap);
    /*! Reboot scanner
     * \return true on success
     */
    bool reboot();
    /*! Power off scanner
     * \return true on success
     */
    bool power_off();
    /*! Override UDP read timeout (default is 250)
     * \param msec      :   timeout in milliseconds
     * \return
     */

#if (__cplusplus >= 201703L)

    template<rf625_param_t t,
             typename T,
             T min_ = std::numeric_limits<T>::min(),
             T max_ = std::numeric_limits<T>::max(),
             T mask_ = std::numeric_limits<T>::max()>
    void setParam(param<t, T, min_, max_, mask_> p)
    {
        auto old = (T*)(m_user_cfg_area + (uint16_t)t);
        *old = (*old & ~mask_) | (p.value() & mask_);
    }


    template<rf625_param_t t,
             typename T,
             T min_ = std::numeric_limits<T>::min(),
             T max_ = std::numeric_limits<T>::max(),
             T mask_ = std::numeric_limits<T>::max()>
    /*!
     * \brief readParam - read params and return tuple with success and param
     */
    auto readParam() {
        param<t, T, min_, max_, mask_> p;
        auto ok = p.setValue(*(T*)(m_user_cfg_area + (uint16_t)t) & mask_);
        return std::make_tuple<ok, p>;
    }

    template<rf625_param_t t,
             typename T,
             T min_ = std::numeric_limits<T>::min(),
             T max_ = std::numeric_limits<T>::max(),
             T mask_ = std::numeric_limits<T>::max()>
    /*!
     * \brief readParam - reads param and throws exception if internal buffer
     * is invalid
     */
    param<t, T, min_, max_, mask_> readParam() {
        return param<t, T, min_, max_, mask_>(*(T*)(m_user_cfg_area + (uint16_t)t) & mask_);
    }

#endif

    /*! UDP functions
     */

    bool udp_set_timeout(int msec);
    /*! Get result profile from scanner's UDP stream
     * \param profile   :   a reference to result rf625_profile structure
     * \return true on success
     */
    bool udp_get_result(rf625_profile& profile);
    /*! Register udp callback function and start udp streaming in background
     * \param proc      :   ptr to callback function
     *                      callback function must be defined as void fn(rf625_profile profile)
     */
    void udp_register_callback(rf625_udp_proc proc);
    /*! Unregister udp callback function and stop background thread
     */
    void udp_unregister_callback();
    /*! Request scanner(s) to immediately send its info block to port 6001
     * \param serial_number :   s/n of scanner or 0 to execute on all scanners over network
     * \return true on success
     */

    /*! Unconnected protocol functions
     */

    static bool up_request_info(uint32_t serial_number);
    /*! Send reboot command to scanner(s)
     * \param serial_number :   s/n of scanner or 0 to execute on all scanners over network
     * \return true on success
     */
    static bool up_reboot(uint32_t serial_number);
    /*! Send power off command to scanner(s)
     * \param serial_number :   s/n of scanner or 0 to execute on all scanners over network
     * \return true on success
     */
    static bool up_power_off(uint32_t serial_number);
    /*! Close TCP connection on scanner(s) if active
     * \param serial_number :   s/n of scanner or 0 to execute on all scanners over network
     * \return true on success
     */
    static bool up_close_tcp_connection(uint32_t serial_number);
    /*! Transfer specific user parameter(s) to scanner's memory
     * \param serial_number :   s/n of scanner
     * \param params        :   ref to rf625_parameters structure
     * \param param_bits    :   bit field of parameters to change (refer to rf625_parameter_bits)
     * \return true on success
     */
    static bool up_write_params(uint32_t serial_number, const rf625_parameters& params, uint32_t param_bits);
    /*! Read user parameters from scanner's memory
     * \param serial_number :   s/n of scanner
     * \param params    :   a reference to rf625_params structure being filled
     * \return true on success
     */
    static bool up_read_params(uint32_t serial_number, rf625_parameters& params);
    /*! Write user parameters from scanner's memory to flash
     * \param serial_number :   s/n of scanner
     * \return true on success
     */
    static bool up_flush_params(uint32_t serial_number);
    /*! Reset user parameters to factory
     * \param serial_number :   s/n of scanner
     * \return true on success
     */
    static bool up_reset_params(uint32_t serial_number);

private:

    SOCKET m_sock,
           m_usock;
    rf625_info m_info;
    rf625_parameters m_params;
    rf625_profile m_profile;
    uint8_t *m_user_cfg_area;
    uint8_t* m_buf;
    rf625_udp_proc m_callback;
    std::thread* m_thread;
    std::mutex* m_callback_mx;
    void udp_proc();

};
