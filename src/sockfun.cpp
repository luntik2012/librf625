#if (!defined __linux__ || defined __MINGW32__)
#include <winsock.h>
#endif

#if (defined _WIN32 && !defined __MINGW32__)
typedef int ssize_t;
#endif

#ifndef __linux__
typedef int socklen_t;
#endif


#if (defined __linux__)
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <memory.h>
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR (-1)
typedef int SOCKET;
#endif
#include <cstdint>

bool recv_data(SOCKET s, char* buf, size_t nbytes, bool accept_incomplete_packet = false, size_t* packet_size = nullptr);
bool send_data(SOCKET s, const char* buf, size_t nbytes);
bool udp_recv_datagram(SOCKET s, char* buf, size_t max_length, size_t* datagram_size, struct sockaddr_in* from_addr = nullptr);
bool set_sock_recvtimeo(SOCKET s, int msec);
bool validate_ip(uint32_t ip);
bool validate_netmask(uint32_t netmask);
bool check_ip_in_subnet(uint32_t ip, uint32_t netmask);
bool validate_ip_with_netmask(uint32_t ip, uint32_t netmask);
bool validate_host_address(uint32_t host_addr);

bool recv_data(SOCKET s, char *buf, size_t nbytes, bool accept_incomplete_packet, size_t *packet_size)
{
    ssize_t nret, nrecv = 0;
    if (!buf || !nbytes) {
        return false;
    }
    do {
        nret = recv(s, &buf[nrecv], ssize_t(nbytes)-nrecv, 0);
        if (nret > 0) {
            nrecv += nret;
        }
    } while(nret != SOCKET_ERROR && !accept_incomplete_packet && (nrecv < int(nbytes)));
    if (accept_incomplete_packet) {
        if (packet_size)
            *packet_size = nrecv;
        return (nrecv > 0);
    }
    else {
        return (nrecv == int(nbytes));
    }
}

bool send_data(SOCKET s, const char *buf, size_t nbytes)
{
    ssize_t nret;
    if (!buf || !nbytes) {
        return false;
    }
    nret = send(s, buf, ssize_t(nbytes), 0);
    return (nret == ssize_t(nbytes));
}

bool udp_send_datagram(SOCKET s, char *buf, int length, sockaddr_in *send_addr)
{
    int nret;
    int tolen = sizeof(sockaddr_in);
    if (!buf || !length) {
        return false;
    }
    nret = sendto(s, buf, length, 0, reinterpret_cast<struct sockaddr*>(send_addr), tolen);
    return (nret == length);
}

bool udp_recv_datagram(SOCKET s, char *buf, size_t max_length, size_t *datagram_size, sockaddr_in *from_addr)
{
    int nret;
    socklen_t from_size;
    struct sockaddr_in from_addr_l;
    from_size = sizeof(from_addr_l);
    if (s == INVALID_SOCKET) {
        return false;
    }
    nret = recvfrom(s, buf, ssize_t(max_length), 0, reinterpret_cast<struct sockaddr*>(&from_addr_l), &from_size);
    if (nret > 0)
    {
        if (from_addr) {
            memcpy(from_addr, &from_addr_l, sizeof(from_addr_l));
        }
        *datagram_size = nret;
        return true;
    }
    else
    {
        return false;
    }
}

bool set_sock_recvtimeo(SOCKET s, int msec)
{
#ifdef _WIN32
    DWORD t = msec;
#else
    timeval t;
    t.tv_sec = msec / 1000;
    t.tv_usec = (msec % 1000) * 1000;
#endif
    return (setsockopt(s, SOL_SOCKET, SO_RCVTIMEO, reinterpret_cast<char*>(&t), sizeof(t)));
}

bool validate_ip(uint32_t ip)
{
    return ((ip != 0) && (ip != 0xFFFFFFFF) && (ip >> 24 != 0x7F));
}

bool validate_netmask(uint32_t netmask)
{
    uint32_t y = ~netmask;
    uint32_t z = y + 1;
    return ((netmask != 0) && (netmask != 0xFFFFFFFF) && ((z & y) == 0));
}

bool check_ip_in_subnet(uint32_t ip, uint32_t netmask)
{
    uint32_t network_address = ip & netmask;
    uint32_t net_lower = (network_address & netmask);
    uint32_t net_upper = (net_lower | (~netmask));
    return ((ip >= net_lower) && (ip <= net_upper));
}

bool validate_ip_with_netmask(uint32_t ip, uint32_t netmask)
{
    return (
        validate_ip(ip) &&
        validate_netmask(netmask) &&
        check_ip_in_subnet(ip, netmask)
    );
}

bool validate_host_address(uint32_t host_addr)
{
    if ((host_addr >> 24) == 0) //TODO
        return false;
    return true;
}
