/*
 *  File name:  librf625.cpp
 *
 *  Description:    Interface library for Riftek RF625 Laser Scanner
 *
 *  Updated:    2018/01/22
 */
#include "rf625.h"
#include <sys/timeb.h>

#ifdef __MINGW32__
#include <unistd.h>
#endif

/* windows sockets tweaks */
#ifdef _WIN32
extern BOOL WinSockInit();
extern void WinSockDeinit();
extern BOOL EnumAdapterAddresses();
extern void FreeAdapterAddresses();
extern BOOL BindToCompatibleInterface(SOCKET s, in_addr* addr, u_long netMask);
extern u_long GetCompatibleInterface(u_long RemoteIP, u_long SubnetMask);
extern int GetAdaptersCount();
extern const char* GetAdapterAddress(int index);
typedef int socklen_t;
/* !windows sockets tweaks */
#else
#include <memory.h>
#include <arpa/inet.h>
#include <unistd.h>
#define SOCKET_ERROR (-1)
#endif

enum rf625_command: uint32_t
{
    Cmd_GetResult = 0x00000001,
    Cmd_GetImage = 0x00000002,
    Cmd_GetImageBuffer = 0x00000003,
    Cmd_ReadParams = 0x00000004,
    Cmd_WriteParams = 0x00000005,
    Cmd_FlushParams = 0x00000006,
    Cmd_Reboot = 0x00000014,
    Cmd_PowerOff = 0x00000016,
    Cmd_Disconnect = 0x00000019,
    Cmd_Identify = 0x00000021,
    Cmd_RequestInfo = 0x00000040,
    Cmd_CloseTCPConnection = 0x00000041,
    Cmd_SetParams = 0x00000042
};
#define DEFAULT_UDP_RECVTIMEO       250
#define DEFAULT_TCP_RECVTIMEO       4000

static std::mutex _mx;
static const size_t _user_cfg_area_size = 512;
static const size_t _buf_size = 65536;

extern bool recv_data(SOCKET s, char* buf, size_t nbytes, bool accept_incomplete_packet = false, size_t* packet_size = nullptr);
extern bool send_data(SOCKET s, const char* buf, size_t nbytes);
extern bool udp_recv_datagram(SOCKET s, char* buf, size_t max_length, size_t* datagram_size, struct sockaddr_in* from_addr = nullptr);
extern bool set_sock_recvtimeo(SOCKET s, int msec);
extern bool validate_ip(uint32_t ip);
extern bool validate_netmask(uint32_t netmask);
extern bool check_ip_in_subnet(uint32_t ip, uint32_t netmask);
extern bool validate_ip_with_netmask(uint32_t ip, uint32_t netmask);
extern bool validate_host_address(uint32_t host_addr);

bool send_cmd_packet(SOCKET s, rf625_command cmd, uint32_t attach_size = 0, uint32_t offset = 0, uint32_t size = 0, bool reply = false);
bool send_cmd_packet(SOCKET s, rf625_command cmd, uint32_t attach_size, uint32_t offset, uint32_t size, bool reply)
{
    int nret;
    uint32_t blk[4];
    uint16_t rx;
    blk[0] = cmd; blk[1] = attach_size; blk[2] = offset; blk[3] = size;
    nret = send(s, reinterpret_cast<char*>(&blk), sizeof(blk), 0);
    if (nret != sizeof(blk)) {
        return false;
    }
    if (reply) {
        nret = recv(s, reinterpret_cast<char*>(&rx), sizeof(rx), 0);
        return (nret == sizeof(rx) && rx == 0x4B4F);
    }
    return true;
}

bool rf625::init()
{
    bool res = true;
#if (defined _WIN32 && !defined __MINGW32__)
    if (!WinSockInit()) {
        res = false;
    }
    EnumAdapterAddresses();
#endif
    return res;
}

void rf625::cleanup()
{
#if (defined _WIN32 && !defined __MINGW32__)
    FreeAdapterAddresses();
    WinSockDeinit();
#endif
}

rf625_list rf625::search(int time_ms)
{
    const size_t buf_size = 268;
    const int recv_timeo = 250;
    const u_short port_number = 6001;
    const uint16_t device_type = 625;
    SOCKET s;
    struct sockaddr_in recv_addr;
    struct sockaddr_in from_addr;
    socklen_t from_len;
    rf625_list result;
    struct timeb start_t, now_t;
    int nret;
    size_t i;
    char* buf = nullptr;
    s = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (s == SOCKET_ERROR) {
        return result;
    }
    set_sock_recvtimeo(s, recv_timeo);
    recv_addr.sin_family = AF_INET;
    recv_addr.sin_port = htons(port_number);
    recv_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    nret = bind(s, reinterpret_cast<struct sockaddr*>(&recv_addr), sizeof(recv_addr));
    if (nret == SOCKET_ERROR) {
#ifdef _WIN32
        nret = WSAGetLastError();
        if (nret == WSAEADDRINUSE) {
#endif
            nret = 1;
            setsockopt(s, SOL_SOCKET, SO_REUSEADDR, reinterpret_cast<char *>(&nret), sizeof(nret));
#ifdef _WIN32
        }
#endif
        nret = bind(s, reinterpret_cast<struct sockaddr*>(&recv_addr), sizeof(recv_addr));
        if (nret == SOCKET_ERROR) {
            return result;
        }
    }
    buf = new char[buf_size];
    ftime(&start_t);
    do
    {
        from_len = sizeof(from_addr);
        nret = recvfrom(s, buf, buf_size, 0, reinterpret_cast<struct sockaddr*>(&from_addr), &from_len);
        if (nret == buf_size) {
            if (!memcmp(&buf[0], &device_type, sizeof(device_type))) {
                rf625_info info;
                memset(&info, 0, sizeof(info));
                memcpy(&info.serial_number, &buf[12+1], 3);
                for (i=0; i<=result.size(); i++) {
                    if (i<result.size() && result.at(i)->get_info().serial_number == info.serial_number)
                        break;
                }
                if (i<=result.size())
                    continue;
                memcpy(&info.hardware_address, &buf[6], 6);
                memcpy(&info.ip_address, &buf[2], 4);
                memcpy(&info.subnet_mask, &buf[12+196], 4);
                memcpy(&info.tcp_port, &buf[12+224], 2);
                memcpy(&info.udp_port, &buf[12+220], 2);
                info.dhs_capable = ((buf[12] & 0x80) != 0);
                switch (buf[12+20]) {
                case 1:
                    info.laser_color = Red; break;
                case 2:
                    info.laser_color = Blue; break;
                case 3:
                    info.laser_color = Green; break;
                case 4:
                    info.laser_color = Infrared; break;
                default:
                    info.laser_color = Red; break;
                }
                memcpy(&info.z_begin, &buf[12+4], 2);
                memcpy(&info.z_range, &buf[12+6], 2);
                memcpy(&info.x_smr, &buf[12+8], 2);
                memcpy(&info.x_emr, &buf[12+10], 2);
                memcpy(&info.uimage_version, &buf[12+16], 4);
                memcpy(&info.core_a_version, &buf[12+21], 4);
                memcpy(&info.core_b_version, &buf[12+25], 4);
                memcpy(&info.fpga_version, &buf[12+29], 4);
                memcpy(&info.uptime, &buf[12+228], 4);
                info.tcp_connection_active = (buf[12+202] != 0);
                result.push_back(std::shared_ptr<rf625>(new rf625(info)));
            }
        }
        ftime(&now_t);
    } while ((now_t.time - start_t.time) * 1000 + (int(now_t.millitm) - int(start_t.millitm)) < time_ms);
#ifdef _WIN32
    shutdown(s, 0x02); /* SD_BOTH */
    closesocket(s);
#else
    close(s);
#endif
    delete[] buf;
    return result;
}

rf625::rf625(const rf625_info &info)
    : m_sock(INVALID_SOCKET)
    , m_usock(INVALID_SOCKET)
    , m_user_cfg_area(nullptr)
    , m_buf(nullptr)
    , m_callback(nullptr)
    , m_thread(nullptr)
    , m_callback_mx(new std::mutex())
{
    int nret;
    memcpy(&m_info, &info, sizeof(m_info));
    m_user_cfg_area = new uint8_t[_user_cfg_area_size];
    m_buf = new uint8_t[_buf_size];
    memset(m_user_cfg_area, 0, _user_cfg_area_size);
    memset(m_buf, 0, _buf_size);
    // Initialize UDP socket right here
    m_usock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (m_usock != INVALID_SOCKET)
    {
        udp_set_timeout(DEFAULT_UDP_RECVTIMEO);
        nret = 1048576;
        setsockopt(m_usock, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char *>(&nret), sizeof(nret));
        sockaddr_in sin;
        sin.sin_family = AF_INET;
#if (defined _WIN32 && !defined __MINGW32__)
        sin.sin_addr.s_addr = GetCompatibleInterface(info.ip_address, info.subnet_mask);
#else
        sin.sin_addr.s_addr = INADDR_ANY;
#endif
        sin.sin_port = htons(info.udp_port);
        nret = bind(m_usock, reinterpret_cast<struct sockaddr*>(&sin), sizeof(sin));
        if (nret == SOCKET_ERROR) {
#if (defined _WIN32 && !defined __MINGW32__)
            closesocket(m_usock);
#else
            close(m_usock);
#endif
            m_usock = INVALID_SOCKET;
        }
    }
}

rf625::~rf625()
{
    if (is_connected())
        disconnect();

    if (m_usock != INVALID_SOCKET) {
#ifdef _WIN32
        closesocket(m_usock);
#else
        close(m_usock);
#endif
    }

    delete[] m_user_cfg_area;
    delete[] m_buf;
}

const rf625_info &rf625::get_info() const
{
    return m_info;
}

bool rf625::connect()
{
    const int recv_timeo = DEFAULT_TCP_RECVTIMEO;
    int nret;

    if (!m_info.ip_address || !m_info.tcp_port)
        return false;

    if (is_connected())
        return false;

    m_sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

    if (m_sock == SOCKET_ERROR)
        return false;

    set_sock_recvtimeo(m_sock, recv_timeo);
    nret = 1048576;
    setsockopt(m_sock, SOL_SOCKET, SO_RCVBUF, reinterpret_cast<char *>(&nret), sizeof(nret));
#if (defined _WIN32 && !defined __MINGW32__)
    nret = 1;
    setsockopt(m_sock, IPPROTO_TCP, TCP_NODELAY, reinterpret_cast<char *>(&nret), sizeof(nret));
#endif
    sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = m_info.ip_address;
    sin.sin_port = htons(m_info.tcp_port);
#if (defined _WIN32 && !defined __MINGW32__)
    BindToCompatibleInterface(m_sock, &sin.sin_addr, m_info.subnet_mask);
#endif
    nret = ::connect(m_sock, reinterpret_cast<struct sockaddr*>(&sin), sizeof(sin));

    if (nret == SOCKET_ERROR)
        return false;

    _mx.lock();
    if (!send_cmd_packet(m_sock, Cmd_Identify, 0, RF625_API_VERSION)) {
        _mx.unlock();
        disconnect();
        return false;
    }
    _mx.unlock();

    read_params(m_params);

    return true;
}

bool rf625::disconnect()
{
    if (!is_connected())
        return false;

    _mx.lock();

    if (!send_cmd_packet(m_sock, Cmd_Disconnect)) {
        _mx.unlock();
        return false;
    }

    _mx.unlock();
#ifdef _WIN32
    shutdown(m_sock, 0x02); /* SD_BOTH */
    closesocket(m_sock);
#else
    close(m_sock);
#endif
    m_sock = INVALID_SOCKET;

    return true;
}

static void map_cfg_area_to_params(uint8_t* cfg_area, rf625_parameters &params)
{
    uint16_t v;
    params.laser_enable = (cfg_area[68] != 0);
    params.laser_level = cfg_area[2];
    memcpy(&params.exposure_time, &cfg_area[3], 2);
    memcpy(&params.roi_top, &cfg_area[5], 2);
    memcpy(&params.roi_height, &cfg_area[7], 2);
    memcpy(&v, &cfg_area[9], 2);

    switch (v) {
    case 0:
        params.trigger = Max_Frequency; break;
    case 1:
        params.trigger = Custom_Frequency; break;
    case 2:
        params.trigger = External_Input; break;
    case 3:
        params.trigger = Encoder; break;
    case 4:
        params.trigger = Step_Dir; break;
    default:
        break;
    }

    memcpy(&params.ext_sync_divisor, &cfg_area[11], 2);
    params.ip_address = static_cast<uint32_t>(cfg_area[16])
            | (static_cast<uint32_t>(cfg_area[15]) << 8)
            | (static_cast<uint32_t>(cfg_area[14]) << 16)
            | (static_cast<uint32_t>(cfg_area[13]) << 24);
    params.subnet_mask = static_cast<uint32_t>(cfg_area[20])
            | (static_cast<uint32_t>(cfg_area[19]) << 8)
            | (static_cast<uint32_t>(cfg_area[18]) << 16)
            | (static_cast<uint32_t>(cfg_area[17]) << 24);
    params.udp_host_address = static_cast<uint32_t>(cfg_area[24])
            | (static_cast<uint32_t>(cfg_area[23]) << 8)
            | (static_cast<uint32_t>(cfg_area[22]) << 16)
            | (static_cast<uint32_t>(cfg_area[21]) << 24);
    memcpy(&params.udp_port, &cfg_area[25], 2);
    memcpy(&params.udp_frequency, &cfg_area[27], 2);
    memcpy(&params.tcp_port, &cfg_area[29], 2);
    params.pixel_brightness_thres = cfg_area[32];
    params.diff_brightness_thres = cfg_area[33];
    params.raw_image = (cfg_area[34] != 0);

    params.resolution = rf625_resolution(cfg_area[int(rf625_param_t::x_resolution)]);

    params.dhs_enable = (cfg_area[36] != 0);
    memcpy(&v, &cfg_area[38], 2);

    if ((v & 0x000F) == 0x00) params.sync_input_channel = 0;
    else if ((v & 0x000F) == 0x01) params.sync_input_channel = 1;
    else if ((v & 0x000F) == 0x02) params.sync_input_channel = 2;
    else if ((v & 0x000F) == 0x04) params.sync_input_channel = 3;
    else if ((v & 0x000F) == 0x08) params.sync_input_channel = 4;

    params.measure_sync_enable = (cfg_area[40] != 0);

    if ((v & 0x0F00) == 0x0000) params.measurement_sync_input_channel = 0;
    else if ((v & 0x0F00) == 0x0100) params.measurement_sync_input_channel = 1;
    else if ((v & 0x0F00) == 0x0200) params.measurement_sync_input_channel = 2;
    else if ((v & 0x0F00) == 0x0400) params.measurement_sync_input_channel = 3;
    else if ((v & 0x0F00) == 0x0800) params.measurement_sync_input_channel = 4;
    if ((v & 0x00F0) == 0x0000) params.output_channel = 0;
    else if ((v & 0x00F0) == 0x0010) params.output_channel = 1;
    else if ((v & 0x00F0) == 0x0020) params.output_channel = 2;

    params.tcp_autodisconnect_enable = (cfg_area[46] != 0);
    memcpy(&params.tcp_autodisconnect_timeout, &cfg_area[44], 2);
    params.udp_stream_enable = (cfg_area[55] != 0);
    params.encoder_has_zero = (cfg_area[67] != 0);
    params.bracketing_enable = (cfg_area[69] != 0);
    memcpy(&params.bracketing_exposure_0, &cfg_area[70], 2);
    memcpy(&params.bracketing_exposure_1, &cfg_area[72], 2);
    params.fixed_frame_enable = (cfg_area[75] != 0);
}

bool rf625::read_params(rf625_parameters &params)
{
    if (!is_connected())
        return false;

    // jic
    if (!m_user_cfg_area)
        return false;

    _mx.lock();

    if (!send_cmd_packet(m_sock, Cmd_ReadParams)) {
        _mx.unlock();
        return false;
    }

    if (!recv_data(m_sock, reinterpret_cast<char*>(m_user_cfg_area), _user_cfg_area_size))
        return false;

    _mx.unlock();

    map_cfg_area_to_params(m_user_cfg_area, params);

    if (&params != &m_params)
        memcpy(const_cast<rf625_parameters*>(&params), &m_params, sizeof(params));

    return true;
}

bool rf625::validate_params(const rf625_parameters &params, uint32_t *error_bits)
{
    uint32_t eb = 0;

    if (params.exposure_time > 3824)
        eb |= Exposure_Time;
    if (params.roi_top % 32 || params.roi_top > 448)
        eb |= ROI_Top;
    if (params.roi_height % 32 || params.roi_height < 32 || params.roi_height > (480-params.roi_top))
        eb |= ROI_Height;
    if (params.trigger > Step_Dir)
        eb |= Trigger;
    if (!validate_ip_with_netmask(params.ip_address, params.subnet_mask))
        eb |= IP_Address;
    if (!validate_netmask(params.subnet_mask))    // does it make sense after previous check?
        eb |= Subnet_Mask;
    if (!validate_host_address(params.udp_host_address))
        eb |= UDP_Host_Address;
    if (!params.udp_port || params.udp_port == 6001)
        eb |= UDP_Port;
    if (!params.udp_frequency || params.udp_frequency > (params.dhs_enable ? 500 : 250))
        eb |= UDP_Frequency;
    if (!params.tcp_port || params.tcp_port == 6001)
        eb |= TCP_Port;
    if (params.resolution < rf625_resolution::Res_80 || params.resolution > rf625_resolution::Res_1280)
        eb |= Resolution;
    if (params.sync_input_channel > 4)
        eb |= Sync_Input_Channel;
    if (params.measurement_sync_input_channel > 4)
        eb |= Measurement_Sync_Input_Channel;
    if (params.output_channel > 2)
        eb |= Output_Channel;
    if (params.bracketing_exposure_0 > 3824)
        eb |= Bracketing_Exposure_0;
    if (params.bracketing_exposure_0 > 3824)
        eb |= Bracketing_Exposure_1;
    if (error_bits)
        *error_bits = eb;

    return (eb == 0);
}

static void map_params_to_cfg_area(const rf625_parameters &params, uint8_t* cfg_area)
{
    uint16_t v;
    cfg_area[68] = params.laser_enable;
    cfg_area[2] = params.laser_level;
    memcpy(&cfg_area[3], &params.exposure_time, 2);
    memcpy(&cfg_area[5], &params.roi_top, 2);
    memcpy(&cfg_area[7], &params.roi_height, 2);

    switch (params.trigger) {
    case Max_Frequency:
        v=0; break;
    case Custom_Frequency:
        v=1; break;
    case External_Input:
        v=2; break;
    case Encoder:
        v=3; break;
    case Step_Dir:
        v=4; break;
    default:
        break;
    }

    memcpy(&cfg_area[9], &v, 2);
    memcpy(&cfg_area[11], &params.ext_sync_divisor, 2);
    cfg_area[16] = params.ip_address & 0xFF;
    cfg_area[15] = (params.ip_address >> 8) & 0xFF;
    cfg_area[14] = (params.ip_address >> 16) & 0xFF;
    cfg_area[13] = (params.ip_address >> 24) & 0xFF;
    cfg_area[20] = params.subnet_mask & 0xFF;
    cfg_area[19] = (params.subnet_mask >> 8) & 0xFF;
    cfg_area[18] = (params.subnet_mask >> 16) & 0xFF;
    cfg_area[17] = (params.subnet_mask >> 24) & 0xFF;
    cfg_area[24] = params.udp_host_address & 0xFF;
    cfg_area[23] = (params.udp_host_address >> 8) & 0xFF;
    cfg_area[22] = (params.udp_host_address >> 16) & 0xFF;
    cfg_area[21] = (params.udp_host_address >> 24) & 0xFF;
    memcpy(&cfg_area[25], &params.udp_port, 2);
    memcpy(&cfg_area[27], &params.udp_frequency, 2);
    memcpy(&cfg_area[29], &params.tcp_port, 2);
    cfg_area[32] = params.pixel_brightness_thres;
    cfg_area[33] = params.diff_brightness_thres;
    cfg_area[34] = params.raw_image;

    cfg_area[int(rf625_param_t::x_resolution)] = uint8_t(params.resolution);

    cfg_area[36] = params.dhs_enable;
    memcpy(&v, &cfg_area[38], 2);
    v &= 0xF000;
    v |= (0x0001 << params.sync_input_channel);
    v |= (0x0100 << params.measurement_sync_input_channel);
    v |= (0x0010 << params.output_channel);
    memcpy(&cfg_area[38], &v, 2);
    cfg_area[46] = params.tcp_autodisconnect_enable;
    memcpy(&cfg_area[44], &params.tcp_autodisconnect_timeout, 2);
    cfg_area[55] = params.udp_stream_enable;
    cfg_area[67] = params.encoder_has_zero;
    cfg_area[69] = params.bracketing_enable;
    memcpy(&cfg_area[70], &params.bracketing_exposure_0, 2);
    memcpy(&cfg_area[72], &params.bracketing_exposure_1, 2);
    cfg_area[75] = params.fixed_frame_enable;
}

bool rf625::write_params(const rf625_parameters &params)
{
    if (!is_connected()) {
        return false;
    }
    if (!m_user_cfg_area) {  // jic
        return false;
    }
    /*    if (!validate_params(params)) {   // do we need to check it there?
        return false;
    } */
    map_params_to_cfg_area(params, m_user_cfg_area);
    if (&params != &m_params) {
        memcpy(&m_params, const_cast<rf625_parameters*>(&params), sizeof(params));
    }
    _mx.lock();
    if (!send_cmd_packet(m_sock, Cmd_WriteParams, _user_cfg_area_size, 1, 0)) {
        _mx.unlock();
        return false;
    }
    if (!send_data(m_sock, reinterpret_cast<char*>(m_user_cfg_area), _user_cfg_area_size)) {
        _mx.unlock();
        return false;
    }
    uint16_t rx;
    if (!recv_data(m_sock, reinterpret_cast<char*>(&rx), 2)) {
        _mx.unlock();
        return false;
    }
    _mx.unlock();
    return (rx == 0x4B4F);
}

bool rf625::flush_params()
{
    if (!is_connected()) {
        return false;
    }
    _mx.lock();
    if (!send_cmd_packet(m_sock, Cmd_FlushParams, 0, 0, 0, true)) {
        _mx.unlock();
        return false;
    }
    _mx.unlock();
    return true;
}

bool rf625::reset_params()
{
    if (!is_connected()) {
        return false;
    }
    _mx.lock();
    if (!send_cmd_packet(m_sock, Cmd_FlushParams, 0, 1, 0, true)) {
        _mx.unlock();
        return false;
    }
    _mx.unlock();
    return true;
}

/*! One function for both TCP and UDP packets
 * \return true on valid packets
 * \param buf       :   a ptr to packet's data buffer
 * \param buf_size  :   data buffer size
 * \param profile   :   a ref to output ref625_profile struct
 */
static bool process_result(const uint8_t* buf, size_t buf_size, rf625_info& info, rf625_profile &profile)
{
    uint8_t proto_revision = 0;
    uint16_t npts, nextlen, crc;
    const uint8_t* pext = nullptr;
    register int i;
    profile.serial_number = 0;
    profile.x_emr = 0;
    profile.z_range = 0;
    profile.source_ip = 0;
    profile.source_port = 0;
    profile.destination_port = 0;
    profile.points.clear();
    if (!buf || !buf_size) {    // jic
        return false;
    }
    if (buf[9] == 0xFF) {
        proto_revision = buf[8];
    }
    if (proto_revision < 2) {
        return false;   // we don't support fossil formats anymore
    }
    memcpy(&profile.measure_cnt, &buf[0], 2);
    memcpy(&profile.packet_cnt, &buf[2], 2);
    memcpy(&profile.timestamp, &buf[4], 4);
    memcpy(&npts, &buf[10], 2);
    if (buf_size < size_t(12 + npts*4 + 2)) {
        return false;
    }
    memcpy(&nextlen, &buf[12 + npts*4], 2);
    if ((nextlen > 512) || (buf_size < size_t(12 + npts*4 + 2 + nextlen + 2))) {
        return false;
    }
    memcpy(&crc, &buf[12 + npts*4 + 2 + nextlen], 2);
    if (nextlen > 0) {
        pext = &buf[12 + npts*4 + 2];
        switch (pext[0]) {
        case 0x01:  // UDP extended record
            if (nextlen >= 8) {
                memcpy(&profile.serial_number, &pext[1], 3);
                memcpy(&profile.x_emr, &pext[4], 2);
                memcpy(&profile.z_range, &pext[6], 2);
                if (nextlen >= 10) {
                    memcpy(&profile.dir, &pext[8], 2);
                }
            }
            break;
        case 0xFF:  // No data; no fail with empty result (FIXME?)
            return true;
        }
    }
    for (i=0; i<npts; i++) {
        rf625_point pt;
        int16_t x;
        uint16_t z;
        memcpy(&x, &buf[12 + i*2], 2);
        memcpy(&z, &buf[12 + (i+npts)*2], 2);
        if (!profile.x_emr) {
            pt.x = static_cast<double>(x) * info.x_emr / 16384.0;
        }
        else {
            pt.x = static_cast<double>(x) * profile.x_emr / 16384.0;
        }
        if (!profile.z_range) {
            pt.z = static_cast<double>(z) * info.z_range / 16384.0;
        }
        else {
            pt.z = static_cast<double>(z) * profile.z_range / 16384.0;
        }
        profile.points.push_back(pt);
    }
    profile.source_ip = info.ip_address;
    profile.source_port = info.tcp_port;
    return true;
}

bool rf625::get_result(rf625_profile &profile)
{
    const size_t max_packet_size = 10768;
    size_t nrecv;
    if (!is_connected()) {
        return false;
    }
    _mx.lock();
    if (!send_cmd_packet(m_sock, Cmd_GetResult)) {
        _mx.unlock();
        return false;
    }
    if (!recv_data(m_sock, reinterpret_cast<char*>(m_buf), max_packet_size, true, &nrecv)) {
        _mx.unlock();
        return false;
    }
    _mx.unlock();
    if (!process_result(m_buf, nrecv, m_info, profile)) {
        return false;
    }
    return true;
}

bool rf625::get_image(uint8_t *ppixmap)
{
    const size_t image_size = 640 * 480;
    const size_t frame_size = 32768 - 2;
    const uint8_t nframes = image_size / frame_size + 1;
    uint8_t nframe = 0xFF;
    uint32_t offset = 0;
    uint32_t chunk_size;
    uint8_t nimage;
    uint8_t nreq = 0;
    if (!is_connected()) {
        return false;
    }
    if (!ppixmap) {
        return false;
    }
    memset(ppixmap, 0, image_size);
    uint8_t* pframe = new uint8_t[frame_size + 2];
    if (!send_cmd_packet(m_sock, Cmd_GetImage)) {
        delete[] pframe;
        return false;
    }
    _mx.lock();
    while (offset < image_size)
    {
        if ((offset + frame_size) > image_size)
            chunk_size = image_size - offset;
        else
            chunk_size = frame_size;
        if (!send_cmd_packet(m_sock, Cmd_GetImageBuffer, 0, offset, chunk_size)) {
            delete[] pframe;
            _mx.unlock();
            return false;
        }
        if (!recv_data(m_sock, reinterpret_cast<char*>(pframe), chunk_size + 2))
        {
            delete[] pframe;
            _mx.unlock();
            return false;
        }
        if (nframe == 0xFF) {
            nimage = pframe[0];
        }
        if (nimage == pframe[0])
        {
            nframe = pframe[1];
            if (nframe < nframes)
            {
                memcpy(&ppixmap[static_cast<size_t>(nframe) * frame_size], &pframe[2], chunk_size);
            }
            else {
                delete[] pframe;
                _mx.unlock();
                return false;
            }
        }
        offset += chunk_size;
        if (++nreq > (nframes * 2)) {
            delete[] pframe;
            _mx.unlock();
            return false;
        }
    }
    _mx.unlock();
    delete[] pframe;
    return true;
}

bool rf625::reboot()
{
    if (!is_connected())
        return false;

    std::lock_guard<std::mutex> g(_mx);

    if (!send_cmd_packet(m_sock, Cmd_Reboot))
        return false;

    return true;
}

bool rf625::power_off()
{
    if (!is_connected())
        return false;

    std::lock_guard<std::mutex> g(_mx);

    if (!send_cmd_packet(m_sock, Cmd_PowerOff))
        return false;

    return true;
}

bool rf625::udp_set_timeout(int msec)
{
    int nret;
    nret = set_sock_recvtimeo(m_usock, msec);

    return (nret != SOCKET_ERROR);
}

bool rf625::udp_get_result(rf625_profile &profile)
{
    struct sockaddr_in from_addr;
    const size_t max_packet_size = 10768;
    size_t nrecv;

    if (!udp_recv_datagram(m_usock, reinterpret_cast<char*>(m_buf), max_packet_size, &nrecv, &from_addr))
        return false;

    profile.source_ip = ntohl(from_addr.sin_addr.s_addr);
    profile.source_port = ntohs(from_addr.sin_port);
    profile.destination_port = m_params.udp_port;

    if (!process_result(m_buf, nrecv, m_info, profile))
        return false;

    return true;
}

void rf625::udp_proc()
{
    rf625_profile profile;

    while(m_callback != nullptr)
    {
        if (udp_get_result(profile)) {
            if(m_callback_mx->try_lock())
            {
                if (m_callback)
                    m_callback(profile);

                m_callback_mx->unlock();
            }
        }
    }
}

void rf625::udp_register_callback(rf625_udp_proc proc)
{
    if(m_callback)
        udp_unregister_callback();

    std::lock_guard<std::mutex> g(*m_callback_mx);

    m_callback = proc;
    m_thread = new std::thread(&rf625::udp_proc, this);
}

void rf625::udp_unregister_callback()
{
    m_callback_mx->lock();

    if (m_callback) {
        m_callback = nullptr;
        m_thread->join();
        delete m_thread;
        m_thread = nullptr;
    }

    m_callback_mx->unlock();
}

/*! Main routine for processing broadcast commands
 * \param serial_number :   Serial number of destination scanner (may be 0 to send to all scanners over network)
 * \param command       :   Command code
 * \param pparams       :   Ptr to array of data to send as attachment
 * \param params_length :   Length of attachment
 * \param pansw         :   Ptr to array to store an answer from scanner if applicable
 * \param answ_length   :   Length of answer
 * \return
 */
static bool send_broadcast_command(
        uint32_t serial_number,
        uint32_t command,
        const void* pparams = nullptr,
        uint32_t params_length = 0,
        void* pansw = nullptr,
        uint32_t answ_length = 0
        );
static bool send_broadcast_command(
        uint32_t serial_number,
        uint32_t command,
        const void* pparams,
        uint32_t params_length,
        void* pansw,
        uint32_t answ_length
        )
{
    SOCKET ssnd, srcv;
    int nret;
    const int recv_timeo = 4000;
    const u_long port_number_recv = 62588,
            port_number_send = 62533;
    struct sockaddr_in sin, send_addr;
    uint32_t single_param;
    bool ret = false;
    char cmd_packet[16];
    if (answ_length)
    {
        if (!pansw) {
            return false;
        }
        srcv = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (srcv == INVALID_SOCKET) {
            return false;
        }
        ssnd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (ssnd == INVALID_SOCKET) {
#ifdef _WIN32
            closesocket(srcv);
#else
            close(srcv);
#endif
            return false;
        }
        set_sock_recvtimeo(srcv, recv_timeo);
        sin.sin_family = AF_INET;
        sin.sin_port = htons(port_number_recv);
        sin.sin_addr.s_addr = htonl(INADDR_ANY);
        nret = bind(srcv, reinterpret_cast<struct sockaddr*>(&sin), sizeof(sin));
        if (nret == SOCKET_ERROR) {
#ifdef _WIN32
            closesocket(srcv);
#else
            close(srcv);
#endif
            return false;
        }
    }
    send_addr.sin_family = AF_INET;
    send_addr.sin_addr.s_addr = htonl(INADDR_BROADCAST);
    send_addr.sin_port = htons(port_number_send);
    memset(cmd_packet, 0, 16);
    memcpy(&cmd_packet[0], &command, 4);
    if (params_length > 1)
        memcpy(&cmd_packet[4], &params_length, 4);
    single_param = (params_length == 1) ? *reinterpret_cast<const uint32_t*>(pparams) : 0;
    memcpy(&cmd_packet[8], &serial_number, 4);
    memcpy(&cmd_packet[12], &single_param, 4);
    _mx.lock();
#if (defined _WIN32 && !defined __MINGW32__)
    for (register int i=0; i<GetAdaptersCount(); i++)
#endif
    {
        ssnd = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
        if (ssnd == INVALID_SOCKET)
#if (defined _WIN32 && !defined __MINGW32__)
            continue;
#else
            return false;
#endif
        nret = 1;
        setsockopt(ssnd, SOL_SOCKET, SO_BROADCAST, reinterpret_cast<char*>(&nret), sizeof(nret));
        sin.sin_family = AF_INET;
        sin.sin_port = 0;
#if (defined _WIN32 && !defined __MINGW32__)
        sin.sin_addr.s_addr = inet_addr(GetAdapterAddress(i));
        if (sin.sin_addr.s_addr == 0x0100007F)
            continue;
#else
        sin.sin_addr.s_addr = INADDR_ANY;
#endif
        nret = bind(ssnd, reinterpret_cast<struct sockaddr*>(&sin), sizeof(sin));
        if (nret != SOCKET_ERROR) {
            nret = sendto(ssnd, cmd_packet, 16, 0, reinterpret_cast<struct sockaddr*>(&send_addr), sizeof(send_addr));
            if (nret == 16) {
                if (params_length > 1) {
                    nret = sendto(ssnd, reinterpret_cast<const char*>(pparams), params_length, 0, reinterpret_cast<struct sockaddr*>(&send_addr), sizeof(send_addr));
                    if (nret == int(params_length))
                        ret = true;
                }
                else {
                    ret = true;
                }
            }
        }
#ifdef _WIN32
        closesocket(ssnd);
#else
        close(ssnd);
#endif
    }
    if (answ_length)
    {
        nret = recv(srcv, reinterpret_cast<char*>(pansw), answ_length, 0);
    }
    _mx.unlock();
    if (answ_length)
    {
#ifdef _WIN32
        closesocket(srcv);
#else
        close(srcv);
#endif
        return (nret == int(answ_length));
    }
    else {
        return ret;
    }
}

bool rf625::up_request_info(uint32_t serial_number)
{
    return send_broadcast_command(serial_number, Cmd_RequestInfo);
}

bool rf625::up_reboot(uint32_t serial_number)
{
    return send_broadcast_command(serial_number, Cmd_Reboot);
}

bool rf625::up_power_off(uint32_t serial_number)
{
    return send_broadcast_command(serial_number, Cmd_PowerOff);
}

bool rf625::up_close_tcp_connection(uint32_t serial_number)
{
    return send_broadcast_command(serial_number, Cmd_CloseTCPConnection);
}

bool rf625::up_write_params(uint32_t serial_number, const rf625_parameters &params, uint32_t param_bits)
{
    const uint16_t param_offsets[] = {
        68, 2,  3,  5,  7,  9,  11, 13, 17, 21, 29, 27, 32, 33, 34, 35, 36, 38, 40, 38, 41, 38, 46, 44, 55, 67, 69, 70, 72, 75
    };
    const uint8_t param_lens[] = {
        1,  1,  2,  2,  2,  2,  2,  4,  4,  4,  2,  2,  1,  1,  1,  1,  1,  2,  1,  2,  2,  2,  1,  2,  1,  1,  1,  2,  2,  1
    };
    bool ret;
    uint8_t *buf = new uint8_t[_user_cfg_area_size * 2],
            *cfg_area = new uint8_t[_user_cfg_area_size];
    memset(cfg_area, 0, _user_cfg_area_size);
    map_params_to_cfg_area(params, cfg_area);
    int pos = 0;
    uint32_t flag = 1;
    uint8_t params_count = sizeof(param_lens);
    for (uint8_t i=0; i<params_count; i++) {
        if (param_bits & flag) {
            memcpy(&buf[pos], &param_offsets[i], 2);
            buf[pos+2] = param_lens[i];
            memcpy(&buf[pos+3], &cfg_area[param_offsets[i]], param_lens[i]);
            pos += (3 + param_lens[i]);
        }
        flag <<= 1;
    }
    ret = send_broadcast_command(serial_number, Cmd_SetParams, buf, pos);
    delete[] buf;
    delete[] cfg_area;
    return ret;
}

bool rf625::up_read_params(uint32_t serial_number, rf625_parameters &params)
{
    bool ret;
    uint8_t* cfg_area = new uint8_t[_user_cfg_area_size];
    ret = send_broadcast_command(serial_number, Cmd_ReadParams, nullptr, 0, cfg_area, _user_cfg_area_size);
    if (ret) {
        map_cfg_area_to_params(cfg_area, params);
    }
    delete[] cfg_area;
    return ret;
}

bool rf625::up_flush_params(uint32_t serial_number)
{
    uint32_t par = 0;
    return send_broadcast_command(serial_number, Cmd_FlushParams, &par, 1);
}

bool rf625::up_reset_params(uint32_t serial_number)
{
    uint32_t par = 1;
    return send_broadcast_command(serial_number, Cmd_FlushParams, &par, 1);
}
