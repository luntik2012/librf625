#include <iostream>
#include <string.h>

#include "rf625.h"

#ifdef __linux__
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#endif

static bool t_rc = 0;
static bool t_assert(bool fn, const char* fn_name);
void udpproc(rf625_profile profile);
void udpproc2(rf625_profile profile);

int main()
{
    t_assert(rf625::init(), "init");
    std::cout << "Testing data on port 6003" << std::endl;
    rf625_info just_udp_port;
    memset(&just_udp_port, 0, sizeof(just_udp_port));
    just_udp_port.udp_port = 6003;
    rf625 test_dev(just_udp_port);
    rf625_profile profile;
    for (register int i=0; i<110; i++) {
        if (t_assert(test_dev.udp_get_result(profile), "udp_get_result")) {
            std::cout << "sn " << profile.serial_number << " mc " << profile.measure_cnt << " pc " << profile.packet_cnt << " ";
            std::cout << profile.points.size() << " points: [";
            for (register size_t j=0; j<profile.points.size(); j++) {
                if (j<2 || j>(profile.points.size()-3))
                    std::cout << "(" << profile.points.at(j).x << "," << profile.points.at(j).z << ")";
                else if (j==2)
                    std::cout << "...";
            }
            std::cout << "]" << std::endl;
        }
    }
    std::cout << "Searching" << std::endl;
    auto lst = rf625::search(3000);
    rf625_parameters params;
    if (t_assert(lst.size() > 0, "search")) {
        std::cout << "Found " << lst.size() << std::endl;
        auto dev = lst.at(0);
        std::cout << "TCP test" << std::endl;
        if (t_assert(dev->connect(), "connect")) {
            if (t_assert(dev->read_params(params), "read_params")) {
                std::cout << "Current trigger " << (int)params.trigger << std::endl;
                //                params.trigger = (params.trigger == max_frequency) ? external_input : max_frequency;
                //                if (t_assert(dev.write_params(params), "write_params")) {
                //                    std::cout << "New trigger " << (int)params.trigger << std::endl;
                //                }
                if (params.trigger == Max_Frequency) {
                    rf625_profile profile;
                    uint8_t* ppixmap = new uint8_t[640*480];
                    for (register int i=0; i<110; i++) {
                        if (i<100)
                        {
                            if (t_assert(dev->get_result(profile), "get_result")) {
                                std::cout << "mc " << profile.measure_cnt << " pc " << profile.packet_cnt << " ";
                                std::cout << profile.points.size() << " points: [";
                                for (register size_t j=0; j<profile.points.size(); j++) {
                                    if (j<2 || j>(profile.points.size()-3))
                                        std::cout << "(" << profile.points.at(j).x << "," << profile.points.at(j).z << ")";
                                    else if (j==2)
                                        std::cout << "...";
                                }
                                std::cout << "]" << std::endl;
                            }
                        }
                        else
                        {
                            if (t_assert(dev->get_image(ppixmap), "get_image")) {
                                //std::string file_name = "image_" + std::to_string(i) + ".bmp";
                            }
                        }
                    }
                    delete[] ppixmap;
                }
            }
            t_assert(dev->disconnect(), "disconnect");
        }
        std::cout << "UDP test" << std::endl;
        rf625_profile profile;
        for (register int i=0; i<110; i++) {
            if (t_assert(dev->udp_get_result(profile), "udp_get_result")) {
                std::cout << "mc " << profile.measure_cnt << " pc " << profile.packet_cnt << " ";
                std::cout << profile.points.size() << " points: [";
                for (register size_t j=0; j<profile.points.size(); j++) {
                    if (j<2 || j>(profile.points.size()-3))
                        std::cout << "(" << profile.points.at(j).x << "," << profile.points.at(j).z << ")";
                    else if (j==2)
                        std::cout << "...";
                }
                std::cout << "]" << std::endl;
            }
        }
        std::cout << "Testing thread" << std::endl;
        for (register int i=0; i<5; i++)
        {
            params.laser_enable = false;
            params.laser_level = 10;
            dev->up_write_params(8014, params, Laser_Enable);
            dev->udp_unregister_callback();
            dev->udp_register_callback(udpproc);
            std::this_thread::sleep_for(std::chrono::milliseconds(500));
            params.laser_enable = true;
            params.laser_level = 255;
            dev->up_write_params(8014, params, Laser_Enable|Laser_Level);
            std::this_thread::sleep_for(std::chrono::milliseconds(1500));
            dev->udp_register_callback(udpproc2);
            std::this_thread::sleep_for(std::chrono::milliseconds(2000));
            dev->udp_unregister_callback();
        }
    }

    if(!dev->setParam(laser_level_t(123)))
        std::cout << "failed to set param\n";
    if(dev->readParam<laser_level_t>() != 123)
        std::cout << "failed to read param\n";

    rf625::cleanup();
    std::cout << std::endl << "Finished, rc=" << t_rc << std::endl;
    return 0;
}


static bool t_assert(bool fn, const char* fn_name) {
    if (!fn) {
        std::cout << "Failed: " << fn_name << std::endl;
        t_rc = 1;
    }
    else
        std::cout << "OK: " << fn_name << std::endl;
    return fn;
}

void udpproc(rf625_profile profile) {
    std::cout << "proc" << std::endl;
    std::cout << "mc " << profile.measure_cnt << " pc " << profile.packet_cnt << " ";
    std::cout << profile.points.size() << " points: [";
    for (register size_t j=0; j<profile.points.size(); j++) {
        if (j<2 || j>(profile.points.size()-3))
            std::cout << "(" << profile.points.at(j).x << "," << profile.points.at(j).z << ")";
        else if (j==2)
            std::cout << "...";
    }
    std::cout << "]" << std::endl;
}

void udpproc2(rf625_profile profile) {
    std::cout << "proc2" << std::endl;
    struct in_addr in;
    in.s_addr = profile.source_ip;
    std::cout << "sn " << profile.serial_number << " from " << inet_ntoa(in) << std::endl;
}
