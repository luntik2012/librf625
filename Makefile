CC = g++
CXX_FLAGS = -O3 -std=c++11 -fPIC -pthread

DEPENDS = src/rf625.cpp \
	  src/rf625.h \
	  src/sockfun.cpp

librf625.so: ${DEPENDS}
	${CC} -shared ${CXX_FLAGS} -o $@ src/rf625.cpp src/sockfun.cpp


test: librf625.so src/test.cpp
	${CC} ${CXX_FLAGS} src/test.cpp -o $@ -L. -lrf625

sample: librf625.so samples/udp_stream.cpp
	${CC} ${CXX_FLAGS} samples/udp_stream.cpp -I ./src -L. -lrf625 -o $@ -lcurses

clean:
	rm *.so test sample

all: librf625.so test sample

.PHONY : librf625.so test clean all sample
