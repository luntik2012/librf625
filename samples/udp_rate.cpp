#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <sys/timeb.h>
#include "rf625.h"

#ifdef __linux__
#include <arpa/inet.h>
#include <curses.h>
#else
#include <conio.h>
#endif

std::unordered_map<int,std::ofstream*> dump_files;
std::unordered_map<int,uint32_t> count;
struct timeb start_t, end_t;
bool start_timer = false;

void udp_proc(rf625_profile prf)
{
    /* Here we have all neccessary data in prf structure
     * let's write measure and packet counters and number of points to output file */
    std::ofstream *f = dump_files[prf.serial_number];
    *f << prf.measure_cnt << "\t" << prf.packet_cnt << "\t" << prf.points.size() << std::endl;
    /* Increase counter */
    count[prf.serial_number]++;
    if (start_timer) {
	//std::cout << count[prf.serial_number] << std::endl;
        start_timer = false;
        count[prf.serial_number] = 0;
    }
}

int main(int argc, char *argv[])
{
    /* Initialize librf625 */
    rf625::init();

    /* Execute search for scanners */
    std::cout << "Searching..." << std::endl;
    rf625_list dev_list = rf625::search(3000);
    if (dev_list.empty()) {
        std::cout << "Nothing found" << std::endl;
        return 1;
    }
    std::cout << std::endl << "#\t| S/N\t| Address\t| Port" << std::endl;

    /* List scanners info
     * create output stream for each scanner */
    int i;
    for(i=0; i<dev_list.size(); i++)
    {
        auto dev = dev_list.at(i);
        if (!dev) continue;
        in_addr addr;
        uint32_t sn = dev->get_info().serial_number;
        addr.s_addr = dev->get_info().ip_address;
        std::cout << i << "\t| "
                  << sn << "\t| "
                  << inet_ntoa(addr) << "\t| "
                  << dev->get_info().udp_port << std::endl;
        std::ofstream *f = new std::ofstream();
        std::string fname = "dmp_" + std::to_string(sn) + ".txt";
        f->open(fname.data(), std::ofstream::out | std::ofstream::trunc);
        dump_files[sn] = f;
        count[sn] = -500;
    }

    /* Start UDP streaming for each scanner in new thread */
    char c;
    for (auto dev: dev_list) {
        dev->udp_register_callback(udp_proc);
    }
    std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    std::cout << std::endl << "Started measuring rate, press Esc to stop..." << std::endl;
    std::cout << std::flush;
    ftime(&start_t);
    start_timer = true;
    while (true)
    {
        c = getch();
        if (c==27)
            break;  /* Break if Esc key pressed */
    }

    /* Stop threads by unregistering callback function
     * note you can switch between different callback functions without unregistering them */
    for (auto dev: dev_list) {
        dev->udp_unregister_callback();
    }
    ftime(&end_t);
    int time_elapsed = (end_t.time - start_t.time) * 1000 + (int(end_t.millitm) - int(start_t.millitm));
    std::cout << "Finished, time elapsed: " << time_elapsed << " ms" << std::endl;

    /* Cleanup librf625 */
    rf625::cleanup();
    /* close output streams and free memory */
    std::cout << std::endl << "#\t| S/N\t| Count\t| Rate" << std::endl;
    i = 0;
    for (auto pair: dump_files) {
        std::ofstream *f = pair.second;
        f->close();
        delete f;
        int sn = pair.first;
        int rate = floorf(float(count[sn]) * 1000.f / float(time_elapsed) + 0.5f);
        std::cout << i << "\t| "
                  << sn << "\t| "
                  << count[sn] << "\t| "
                  << rate << std::endl;
        i++;
    }
    return 0;
}
