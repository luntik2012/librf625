# https://gitlab.com/riftek/dev/rf625/librf625

QT       -= core gui

# uncomment to build test binary
#CONFIG += test

gcc: CONFIG += c++1z
win32:!gcc {
    DEFINES += NOMINMAX
    QMAKE_CXXFLAGS += -std:c++latest
}

DEFINES += RF625_LIBRARY

TARGET = $$qtLibraryTarget(rf625)
TEMPLATE = lib
#CONFIG += staticlib

test {
    DEFINES += TEST
    TARGET = test
    TEMPLATE = app
    CONFIG   += console
}

DESTDIR = $${PWD}

win32 {
    LIBS += -lws2_32
}

HEADERS += \
    src/rf625.h \
    src/rf625params.h

SOURCES += \
    src/rf625.cpp \
    src/sockfun.cpp \
    src/wshlp.cpp

test: SOURCES += src/test.cpp
