# librf625
Riftek RF625 SDK
================

## Contents

- [Requirements](#requirements)
- [Building](#building)
- [Add the SDK to your project](#add-the-sdk-to-your-project)
- [License](#license)

## Requirements

* Compiler with C++11 support

## Building

##### On Linux (g++)
```
make librf625.so
```
##### On Windows (MSVC)
```
Winmake.bat
```
Or open `msvc2017.sln` and build it in Visual Studio 2017

#### Crosscompilation for windows using MinGW
Assuming you are using arclinux
```sh
make -f Makefile.mingw
```

## Add the SDK to your project

##### Qt

Add `INCLUDEPATH += path_to_library_source` and `LIBS += -Lpath_to_module -l$$qtLibraryTarget(rf625)`

##### MSVC

Use Project Properties dialog to add a path to library source to C/C++, Additional Include Directories and path to rf625.lib file to Librarian, General, Additional Library Directories. Add `rf625.lib` Additional Dependencies on the same page.

Alternatively you may add the sources directly to your project's sources.

## License

This software is distributed under LGPL license

